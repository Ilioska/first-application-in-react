import React from 'react'

export const Link = (props) => {
    return <a href={props.href} target={props.nowa}>{props.linkText}</a>
}