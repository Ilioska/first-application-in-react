import React from 'react'

class List extends React.Component {
    state = {
        color: 'red',
        number: 0,
        colorNumber: {
            red: 0,
            blue: 0,
            green: 0,
        }
    }

    render() {
        return (
            <div>
                <span>ta lista nazywa się {this.props.name}</span>
                <div className='colorDiv' style={{backgroundColor: this.state.color}}>
                    <p>{this.state.colorNumber[this.state.color]}</p>
                    <p>{this.state.number}</p>
                </div>
                <button id='red' onClick={this.changeColor}>Red</button>
                <button id='blue' onClick={this.changeColor}>Blue</button>
                <button id='green' onClick={this.changeColor}>Green</button>
            </div>
        )
    }

    changeColor = (e) => {
        let color = e.target.id

        this.setState(prevState => {
                return {
                    color,
                    colorNumber: {
                        ...prevState.colorNumber,
                        [color]: prevState.colorNumber[color] + 1,
                    },
                    number: prevState.number + 1,
                }
            })


        
    }
}

export default List