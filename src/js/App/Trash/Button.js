import React from 'react'

const Button = (props) => {
    let klasa = props.klasa
    
    if (klasa === undefined) {
        klasa = ''
    }
    console.log(props)
    return <button className={`domyslna klasa ${klasa}`} style={{backgroundColor: props.color}}>{props.buttonTitle}</button>
}

export default Button