import React from 'react'
import Button from './components/Button'
import {Link} from './components/Link' //klamry bo nie jest zaimportowany defaultowo
import Row from './components/Row'
import Icon from './components/Icon'
import Line from './components/LineToText'
import List from './containers/List'
import ListTwo from './containers/ListTwo'
import Position from './containers/Position'

const App = () => {
    const title = "Zaloguj"

    return <div className="dupa">
        element tekstowy
        <Button
            color="#546789"
            height="50px"
            buttonTitle="Baton"
        />
        <Button buttonTitle={title}/>
        <Button 
            klasa="buttonClass"
        />
        <Link
            linkText="tekścior" 
            href="http://www.daam.pl/cv"
            nowa="_blank"
        />
        <p>Do kupienia:</p>
        <Position />
        <List 
            name="Lista początkowa"
        />
        <ListTwo 
            name="Lista następna"
        />
    </div>
}

export default App

/*<div className="row">
<Row className="row">
    <Icon
        icon="jam-plus"
        size="30px"
    />
    <Icon
        icon="jam-check"
        size="30px"
    />
    <Icon
        icon="jam-close"
        size="30px"
    />
</Row> 
</div>*/