import React from 'react'

class ListTwo extends React.Component {
    state = {
        color: 'red',
        nextColor: 'blue',
        number: 0,
    }

    render() {
        return (
            <div>
                <span>ta lista nazywa się {this.props.name}</span>
                <div className='colorDiv' style={{backgroundColor: this.state.color}}>{this.state.number}</div>
                <button onClick={this.changeColor}>Change to {this.state.nextColor}</button>
            </div>
        )
    }

    changeColor = () => {
        this.setState(prevState => {
            return {
                color: prevState.nextColor,
                nextColor: prevState.color,
                number: prevState.number + 1,
            }
        })
    }
}

export default ListTwo