import React from 'react'
import Icon from '../components/Icon'
import Input from '../components/Input'

class Position extends React.Component {
    state = {
        isInputActive: false,
        inputValue: this.props.listElement,
    }

    componentDidUpdate(prevProps) {
        if (this.props.listElement !== prevProps.listElement) {
            this.setState({
                inputValue: this.props.listElement
            })
        }
    }

    render() {
        return (
            <div className="styleList">
                <div className="row">
                    {this.renderIcon()}
                    {this.renderInput()}                  
                </div>
            </div>
        )
    }

    renderInput = () => {
        return this.state.isInputActive ?
            <Input
                inputValue={this.state.inputValue}
                onChangeCallback={this.changeValue}
            />
        :
            <p onClick={this.editInput}>{this.state.inputValue}</p>
    }

    renderIcon = () => {
        let iconType, callback

        if (!this.state.inputValue) {
            iconType = "plus"
            callback = this.activeInput
        }

        if (this.state.inputValue && this.state.isInputActive) {
            iconType = "check"
            callback = this.changeInputToString
        }

        if (this.state.inputValue && !this.state.isInputActive) {
            iconType = "close"
            callback = this.removeInputValue
        }

        if (this.props.isListBought) {
            iconType = "arrow-up"
            callback = this.comebackToListToBuy
        }

        return <Icon 
            icon={iconType}
            size="30px"
            onClick={callback}
        />
    }

    activeInput = () => {
        this.setState({
            isInputActive: true,
            isPlusActive: false,
        })
    }

    changeValue = (e) => {
        const value = e.target.value
        this.setState({
            inputValue: value,
        })
    }

    changeInputToString = () => {
        this.props.listUpdater(this.state.inputValue, this.props.elementIndex)
        this.setState(prevState => {
            return {
                isInputActive: false,
                inputValue: this.props.elementIndex === undefined ? '' : prevState.inputValue
            }
        })
    }

    editInput = () => {
        this.setState({isInputActive: true})
    }

    removeInputValue = () => {
        this.props.removeElement(this.props.elementIndex)
    }

    comebackToListToBuy = () => {
        this.props.revertToElement(this.props.elementIndex)
    }
}

export default Position
