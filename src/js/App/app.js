import React, {Fragment} from 'react'
import ListToBuy from './components/ListToBuy'
import ListBought from './components/ListBought'

class App extends React.Component {
    state = {
        listToBuy: [],
        listBought: [],
    }

    render() {
        return (
            <Fragment>
                <p>LISTA ZAKUPÓW</p>
                <ListToBuy 
                    elements={this.state.listToBuy}
                    listUpdater={this.updateListToBuy}
                    removeElement={this.removeElement}
                />
                <ListBought
                    elements={this.state.listBought}
                    isListBought={true}
                    revertToElement={this.revertToElement}
                />
            </Fragment>
        )
    }
    
    updateListToBuy = (newElement, index) => {
        index === undefined ?
            this.setState(prevState => {
                return {listToBuy: prevState.listToBuy.concat(newElement)}
            })
        :
            this.setState(prevState => {
                prevState.listToBuy.splice(index, 1, newElement) // splice zwraca element usunięty. dlatego jest przed returnem
                return {listToBuy: prevState.listToBuy /*concat - metoda tablicowa, która ŁĄCZY starą tablicę z nową tablicą*/}
            })
    }

    removeElement = (index) => {
        this.setState(prevState => {
            const newListBought = prevState.listBought.concat(prevState.listToBuy.splice(index, 1))
            return {
                listToBuy: prevState.listToBuy,
                listBought: newListBought,
            }
        })
    }

    revertToElement = (index) => {
        console.log(index)
        this.setState(prevState => {
            const newListToBuy = prevState.listToBuy.concat(prevState.listBought.splice(index, 1))
            return {
                listToBuy: newListToBuy,
                listBought: prevState.listBought,
            }
        })
    }
}

export default App