import React from 'react'

const Icon = (props) => {
    //const {size} = props
    return <i className={`jam jam-${props.icon}`} style={{fontSize: props.size}} onClick={props.onClick}></i>
}

export default Icon