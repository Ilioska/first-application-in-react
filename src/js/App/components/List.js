import React from 'react'
import Position from '../containers/Position'

const List = (props) => {
    return (
        <div>
            {props.elements.map((el, index) => (
                <Position 
                    isListBought={props.isListBought}
                    listElement={el}
                    listUpdater={props.listUpdater}
                    elementIndex={index}
                    key={index}
                    removeElement={props.removeElement}
                    revertToElement={props.revertToElement}
                />
            ))}
            {!props.isListBought &&
                <Position 
                    listElement=""
                    listUpdater={props.listUpdater}
                />
            }
        </div>
    )
}

export default List