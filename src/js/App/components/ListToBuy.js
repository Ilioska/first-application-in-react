import React from 'react'
import List from './List'

const ListToBuy = (props) => {
    return (
        <React.Fragment>
            <p>Do kupienia:</p>
            <List 
                elements={props.elements}
                listUpdater={props.listUpdater}
                removeElement={props.removeElement}
            />
        </React.Fragment>
    )
}

export default ListToBuy