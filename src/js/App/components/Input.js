import React from 'react'

const Input = (props) => {
    return (<input style={{width: props.width}} onChange={props.onChangeCallback} value={props.inputValue}/>)
}

export default Input