import React from 'react'
import List from './List'

const ListBought = (props) => {
    return (
        <React.Fragment>
            <p>Kupione:</p>
            <List 
                elements={props.elements}
                isListBought={props.isListBought}
                listUpdater={props.listUpdater}
                revertToElement={props.revertToElement}
            />
        </React.Fragment>
    )
}

export default ListBought